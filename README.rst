.. -*- coding: utf-8 -*-

.. contents:: Tabla de Contenidos

Introducción
============

Este producto contiene un tema `Diazo`_ en Plone 4.3 para el `sitio Web de Canaima GNU/Linux`_. 
Este permite integrar visualmente elementos del Plone CMS, redes sociales, y demás aplicaciones 
Web en una única interfaz de usuario.

Características
===============

- Este tema `Diazo`_ contiene los siguientes archivos de reglas:

  - Por definir.

Instalación
===========
Usted puede leer el archivo ``INSTALL.txt`` dentro del directorio ``docs`` de
este paquete.

Descargas
=========

Usted puede encontrar la versión de desarrollo del paquete ``canaimagnulinux.web.theme``
en el `repositorio Canaima GNU/Linux`_ en Github.com.


Sobre la calidad
================

.. image:: https://d2weczhvl823v0.cloudfront.net/CanaimaGNULinux/canaimagnulinux.web.theme/trend.png
   :alt: Bitdeli badge
   :target: https://bitdeli.com/free

.. image:: https://travis-ci.org/CanaimaGNULinux/canaimagnulinux.web.theme.svg?branch=master
   :alt: Travis-CI badge
   :target: https://travis-ci.org/CanaimaGNULinux/canaimagnulinux.web.theme

.. image:: https://coveralls.io/repos/CanaimaGNULinux/canaimagnulinux.web.theme/badge.png?branch=master
   :alt: Coveralls badge
   :target: https://coveralls.io/r/CanaimaGNULinux/canaimagnulinux.web.theme?branch=master

Autor(es) Original(es)
======================

* Maximiliano Vilchez aka maxudes

* Leonardo J .Caballero G. aka macagua

Colaboraciones impresionantes
=============================

* Axel Díaz aka Axelio

* Dehivis Perez aka dehivix

* Rodrigo Bravo aka goidor

* Eliezer Romero aka eliezerfot123

* Miguel Sanabria aka masc1293

* Ericka Simancas aka erickaackseriam

Para una lista actualizada de todo los colaboradores visite:
https://github.com/canaimagnulinux/canaimagnulinux.web.theme/contributors

.. _Diazo: http://pypi.python.org/pypi/diazo
.. _sitio Web de Canaima GNU/Linux: http://canaima.softwarelibre.gob.ve/
.. _repositorio Canaima GNU/Linux: https://github.com/canaimagnulinux/canaimagnulinux.web.theme